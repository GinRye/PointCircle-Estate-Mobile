var i = 0;

self.addEventListener('message', function(e) {
	var method = e.data.method;
	if(method == 'assembleDbData') {
		var original = e.data.original;
		var data = assembleDbData(original.rows);
		self.postMessage(data);
	}
});

function assembleDbData(rows) {
	var data = {};
	for(var i = 0; i < rows.length; i++) {
		var row = rows[i];
		data[row.id] = row;
	}
	return data;
}

