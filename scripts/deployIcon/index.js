let fs = require("fs");
let xml2js = require('xml2js');

let pathName = "./scripts/deployIcon";
let orgs = {};

let files = fs.readdirSync(pathName, {withFileTypes: true});
files.forEach(file => {
	if(file.isDirectory()) {
		let org = require('./' + file.name + '/index.js');
		orgs[file.name] = org;
		let androidDirPath = pathName + '/' + file.name + '/android'
		let androidFiles = fs.readdirSync(androidDirPath, {withFileTypes: true});
		androidFiles.forEach(androidFile => {
			if(androidFile.isDirectory()) {
				let iconDirPath = androidDirPath + '/' + androidFile.name;
				let iconFiles = fs.readdirSync(iconDirPath, {withFileTypes: true});
				iconFiles.forEach(iconFile => {
					let source = iconDirPath + '/' + iconFile.name;
					let target = './platforms/android/res/' + androidFile.name + '/' + iconFile.name;
					fs.copyFileSync(source, target);
					console.log('copy ' + source + ' -> ' + target);
				});
			}
		});
	}
});

let iconJsFilePath = './www/icon.js';
fs.writeFileSync(iconJsFilePath, 'var iconMap = ' + JSON.stringify(orgs, null, 4), 'utf8'); 
console.log('write ' + iconJsFilePath);

let androidManifestFilePath = './platforms/android/AndroidManifest.xml';
let androidManifestContent = fs.readFileSync(androidManifestFilePath, {encoding: 'utf8'});
var parser = new xml2js.Parser();
var builder = new xml2js.Builder();//用于把json对象解析为xml
parser.parseString(androidManifestContent, function(err, result) {
	let application = result.manifest.application[0];
	application.activity.forEach(activity => {
		activity['intent-filter'] = activity['intent-filter'].filter(intentFilter => {
			if(intentFilter.$ && intentFilter.$["android:label"] == "@string/launcher_name") {
				if(!activity._) {
					activity._ = '';
				}
				activity._ = '<!--' + builder.buildObject(intentFilter) + '-->'
				return false;
			}
			return true;
		});
	});
	application['activity-alias'] = [];
	application['activity-alias'].push({
		"$": {
	        "android:name": '.defaultActivity',
	        "android:enabled": true,
	        "android:exported": true,
	        "android:icon": "@mipmap/icon",
	        "android:label": "@string/app_name",
	        "android:targetActivity": ".MainActivity",
		},
        "intent-filter": [{
        	"action": [{
        		"$": {
	        		"android:name": "android.intent.action.MAIN"	
        		}
        	}],
        	"category": [{
        		"$": {
	        		"android:name": "android.intent.category.LAUNCHER"
        		}
        	}],
        }]
	});
	for(let key in orgs) {
		application['activity-alias'].push({
			"$": {
		        "android:name": '.' + key,
		        "android:enabled": false,
		        "android:exported": true,
		        "android:icon": "@mipmap/" + orgs[key].icon,
		        "android:label": orgs[key].label,
		        "android:targetActivity": ".MainActivity",
			},
	        "intent-filter": [{
	        	"action": [{
	        		"$": {
		        		"android:name": "android.intent.action.MAIN"	
	        		}
	        	}],
	        	"category": [{
	        		"$": {
		        		"android:name": "android.intent.category.LAUNCHER"
	        		}
	        	}],
	        }]
		});
	}
	console.log(JSON.stringify(result, null, 2));
	var outputXml = builder.buildObject(result);
	fs.writeFileSync(androidManifestFilePath, outputXml.toString(), "utf8");
});