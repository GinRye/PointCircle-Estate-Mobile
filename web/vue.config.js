module.exports = {
	publicPath: './',
	outputDir: '../www/webapp',
	assetsDir: 'assets',
    runtimeCompiler: true,
	pages: {
		index: {
			entry: 'src/main.js',
			template: 'src/index.html',
			filename: 'index.html'
		}
	},
	configureWebpack: config => {
		config.devtool = 'inline-source-map'
	},
	devServer: {
		port: 9000
	}
}

