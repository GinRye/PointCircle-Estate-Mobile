var tables = {
	map: {},
	list: []
};
tables.list.push({
    name: 'tb_project',
    columns: [{
        name: 'id', type: 'varchar(100)'
    }, {
        name: 'update_time', type: 'int'
    }, {
        name: 'project_name', type: 'varchar(255)'
    }, {
        name: 'organization_id', type: 'varchar(100)'
    }, {
        name: 'parent_project_id', type: 'varchar(100)'
    }]
});
tables.list.push({
	name: 'tb_bid_section',
	columns: [{
		name: 'id', type: 'varchar(100)'
	}, {
		name: 'update_time', type: 'int'
    }, {
        name: 'bid_section_name', type: 'varchar(255)'
    }, {
        name: 'contractor_id', type: 'varchar(100)'
    }, {
        name: 'project_id', type: 'varchar(100)'
    }, {
        name: 'supervisor_id', type: 'varchar(100)'
    }]
});
tables.list.push({
    name: 'tb_contractor',
    columns: [{
        name: 'id', type: 'varchar(100)'
    }, {
        name: 'update_time', type: 'int'
    }, {
        name: 'contact_phone', type: 'varchar(100)'
    }, {
        name: 'contacter', type: 'varchar(100)'
    }, {
        name: 'contractor_name', type: 'varchar(100)'
    }, {
        name: 'contractor_short_name', type: 'varchar(100)'
    }, {
        name: 'manager', type: 'varchar(100)'
    }, {
        name: 'registration_number', type: 'varchar(100)'
    }, {
        name: 'remark', type: 'varchar(100)'
    }, {
        name: 'social_credit_code', type: 'varchar(100)'
    }, {
        name: 'user_amount', type: 'int'
    }, {
        name: 'contractor_type_id', type: 'varchar(100)'
    }]
});

tables.list.push({
    name: 'tb_contractor_type',
    columns: [{
        name: 'id', type: 'varchar(100)'
    }, {
        name: 'update_time', type: 'int'
    }, {
        name: 'contractor_type_name', type: 'varchar(100)'
    }]
});
tables.list.push({
    name: 'tb_user',
    columns: [{
        name: 'id', type: 'varchar(100)'
    }, {
        name: 'update_time', type: 'int'
    }, {
        name: 'effective_date', type: 'int'
    }, {
        name: 'email', type: 'varchar(100)'
    }, {
        name: 'mobilephone', type: 'varchar(100)'
    }, {
        name: 'realname', type: 'varchar(100)'
    }, {
        name: 'username', type: 'varchar(100)'
    }, {
        name: 'organization_id', type: 'varchar(100)'
    }, {
        name: 'app_permission', type: 'varchar(100)'
    }]
});
tables.list.push({
    name: 'tb_user_project',
    columns: [{
        name: 'id', type: 'varchar(100)'
    }, {
        name: 'update_time', type: 'int'
    }, {
        name: 'project_id', type: 'varchar(100)'
    }, {
        name: 'user_id', type: 'varchar(100)'
    }]
});
tables.list.push({
    name: 'tb_checkquality_batch',
    columns: [{
        name: 'id', type: 'varchar(100)'
    }, {
        name: 'update_time', type: 'int'
    }, {
        name: 'batch_purpose', type: 'varchar(100)'
    }, {
        name: 'batch_type', type: 'varchar(100)'
    }, {
        name: 'name', type: 'varchar(100)'
    }, {
        name: 'repair_deadline', type: 'int(11)'
    }, {
        name: 'bid_section_id', type: 'varchar(100)'
    }, {
        name: 'in_charge_by', type: 'varchar(100)'
    }, {
        name: 'is_close', type: 'bit(1)'
    }]
});
tables.list.push({
    name: 'tb_batch_cc',
    columns: [{
        name: 'id', type: 'varchar(100)'
    }, {
        name: 'update_time', type: 'int'
    }, {
        name: 'batch_id', type: 'varchar(100)'
    }, {
        name: 'user_id', type: 'varchar(100)'
    }]
});
tables.list.push({
    name: 'tb_batch_user',
    columns: [{
        name: 'id', type: 'varchar(100)'
    }, {
        name: 'update_time', type: 'int'
    }, {
        name: 'batch_id', type: 'varchar(100)'
    }, {
        name: 'user_id', type: 'varchar(100)'
    }]
});
tables.list.push({
    name: 'tb_building',
    columns: [{
        name: 'id', type: 'varchar(100)'
    }, {
        name: 'update_time', type: 'int'
    }, {
        name: 'building_name', type: 'varchar(100)'
    }, {
        name: 'bid_section_id', type: 'varchar(100)'
    }, {
        name: 'project_id', type: 'varchar(100)'
    }, {
        name: 'sort_number', type: 'int(11)'
    }]
});
tables.list.push({
    name: 'tb_unit',
    columns: [{
        name: 'id', type: 'varchar(100)'
    }, {
        name: 'update_time', type: 'int'
    }, {
        name: 'door_amount', type: 'int(11)'
    }, {
        name: 'house_number_list', type: 'varchar(100)'
    }, {
        name: 'unit_name', type: 'varchar(100)'
    }, {
        name: 'building_id', type: 'varchar(100)'
    }, {
        name: 'sort_number', type: 'int(11)'
    },]
});
tables.list.push({
    name: 'tb_floor',
    columns: [{
        name: 'id', type: 'varchar(100)'
    }, {
        name: 'update_time', type: 'int'
    }, {
        name: 'floor_name', type: 'varchar(100)'
    }, {
        name: 'is_public_area', type: 'bit(1)'
    }, {
        name: 'is_top', type: 'bit(1)'
    }, {
        name: 'building_id', type: 'varchar(100)'
    }, {
        name: 'sort_number', type: 'int(11)'
    }]
});
tables.list.push({
    name: 'tb_house',
    columns: [{
        name: 'id', type: 'varchar(100)'
    }, {
        name: 'update_time', type: 'int'
    }, {
        name: 'house_complete_name', type: 'varchar(100)'
    }, {
        name: 'house_name', type: 'varchar(100)'
    }, {
        name: 'house_number', type: 'varchar(100)'
    }, {
        name: 'is_public_area', type: 'bit(1)'
    }, {
        name: 'floor_id', type: 'varchar(100)'
    }, {
        name: 'house_type_id', type: 'varchar(100)'
    }, {
        name: 'unit_id', type: 'varchar(100)'
    }]
});
tables.list.push({
    name: 'tb_house_type',
    columns: [{
        name: 'id', type: 'varchar(100)'
    }, {
        name: 'update_time', type: 'int'
    }, {
        name: 'house_type_name', type: 'varchar(100)'
    }, {
        name: 'house_type_structure', type: 'varchar(100)'
    }, {
        name: 'url', type: 'varchar(100)'
    }, {
        name: 'local_path', type: 'varchar(100)'
    }]
});
tables.list.push({
    name: 'tb_problem_class',
    columns: [{
        name: 'id', type: 'varchar(100)'
    }, {
        name: 'update_time', type: 'int'
    }, {
        name: 'code', type: 'varchar(100)'
    }, {
        name: 'level', type: 'int(11)'
    }, {
        name: 'problem_class_name', type: 'varchar(100)'
    }, {
        name: 'type', type: 'varchar(100)'
    }, {
        name: 'parent_problem_class_id', type: 'varchar(100)'
    }]
});
tables.list.push({
    name: 'tb_check_item_guideline',
    columns: [{
        name: 'id', type: 'varchar(100)'
    }, {
        name: 'update_time', type: 'int'
    }, {
        name: 'content', type: 'varchar(4096)'
    }, {
        name: 'passrate_mode', type: 'varchar(100)'
    }, {
        name: 'standard_max', type: 'varchar(100)'
    }, {
        name: 'standard_min', type: 'varchar(100)'
    }, {
        name: 'measurement_area_num', type: 'int'
    }, {
        name: 'per_area_points', type: 'int'
    }, {
        name: 'process_mode', type: 'varchar(100)'
    }, {
        name: 'problem_class_id', type: 'varchar(100)'
    }]
});
tables.list.push({
    name: 'tb_problem',
    columns: [{
        name: 'id', type: 'varchar(100)'
    }, {
        name: 'update_time', type: 'int'
    }, {
        name: 'problem_name', type: 'varchar(100)'
    }, {
        name: 'sort', type: 'int(11)'
    }, {
        name: 'class_id', type: 'varchar(100)'
    }]
});
tables.list.push({
    name: 'tb_bid_section_check_item',
    columns: [{
        name: 'id', type: 'varchar(100)'
    }, {
        name: 'update_time', type: 'int'
    }, {
        name: 'cc', type: 'varchar(5000)'
    }, {
        name: 'check_item_type', type: 'varchar(100)'
    }, {
        name: 'reinspect', type: 'varchar(5000)'
    }, {
        name: 'bid_section_id', type: 'varchar(100)'
    }, {
        name: 'contractor_id', type: 'varchar(100)'
    }, {
        name: 'check_item_id', type: 'varchar(100)'
    }, {
        name: 'repair', type: 'varchar(100)'
    }]
});
tables.list.push({
    name: 'tb_bid_section_contractor',
    columns: [{
        name: 'id', type: 'varchar(100)'
    }, {
        name: 'update_time', type: 'int'
    }, {
        name: 'bid_section_id', type: 'varchar(100)'
    }, {
        name: 'contractor_id', type: 'varchar(100)'
    }]
});
tables.list.push({
    name: 'tb_checkquality_problem',
    columns: [{
        name: 'id', type: 'varchar(255)'
    }, {
        name: 'update_time', type: 'int'
    }, {
        name: 'batch_type', type: 'varchar(20)'
    }, {
        name: 'close_date', type: 'int'
    }, {
        name: 'close_reason', type: 'varchar(255)'
    }, {
        name: 'drawing_file', type: 'varchar(1024)'
    }, {
        name: 'emergency_degree', type: 'varchar(255)'
    }, {
        name: 'image_file', type: 'varchar(2048)'
    }, {
        name: 'place', type: 'varchar(255)'
    }, {
        name: 'problem_values', type: 'varchar(1024)'
    }, {
        name: 'regist_date', type: 'int'
    }, {
        name: 'reinspect_completed_date', type: 'int'
    }, {
        name: 'reinspect_image_file', type: 'varchar(1024)'
    }, {
        name: 'reinspect_remark', type: 'varchar(255)'
    }, {
        name: 'remark', type: 'varchar(255)'
    }, {
        name: 'repair_date', type: 'int'
    }, {
        name: 'repair_deadline', type: 'int'
    }, {
        name: 'repair_image_file', type: 'varchar(2048)'
    }, {
        name: 'repair_remark', type: 'varchar(255)'
    }, {
        name: 'sent_back_date', type: 'int'
    }, {
        name: 'sent_back_image_file', type: 'varchar(1024)'
    }, {
        name: 'sent_back_remark', type: 'varchar(255)'
    }, {
        name: 'sent_back_times', type: 'int'
    }, {
        name: 'small_image_file', type: 'varchar(1024)'
    }, {
        name: 'small_reinspect_image_file', type: 'varchar(1024)'
    }, {
        name: 'small_repair_image_file', type: 'varchar(1024)'
    }, {
        name: 'small_sent_back_image_file', type: 'varchar(1024)'
    }, {
        name: 'status', type: 'varchar(255)'
    }, {
        name: 'building_id', type: 'varchar(255)'
    }, {
        name: 'batch_id', type: 'varchar(255)'
    }, {
        name: 'contractor_id', type: 'varchar(255)'
    }, {
        name: 'floor_id', type: 'varchar(255)'
    }, {
        name: 'room_id', type: 'varchar(255)'
    }, {
        name: 'desc_id', type: 'varchar(255)'
    }, {
        name: 'item_id', type: 'varchar(255)'
    }, {
        name: 'regist_by', type: 'varchar(255)'
    }, {
        name: 'reinspect_by', type: 'varchar(255)'
    }, {
        name: 'repair_by', type: 'varchar(255)'
    }, {
        name: 'sent_back_by', type: 'varchar(255)'
    }, {
        name: 'unit_id', type: 'varchar(255)'
    }, {
        name: 'indexTimeout', type: 'varchar(255)'
    }, {
        name: 'ccs', type: 'varchar(255)'
    }]
});
tables.list.push({
    name: 'tb_checkquality_repair',
    columns: [{
        name: 'id', type: 'varchar(255)'
    }, {
        name: 'update_time', type: 'int'
    }, {
        name: 'has_read', type: 'bit(1)'
    }, {
        name: 'reading_time', type: 'int'
    }, {
        name: 'checkquality_problem_id', type: 'varchar(255)'
    }, {
        name: 'child_checkquality_repair_id', type: 'varchar(255)'
    }, {
        name: 'parent_checkquality_repair_id', type: 'varchar(255)'
    }, {
        name: 'repair', type: 'varchar(255)'
    }]
});
tables.list.push({
    name: 'tb_checkquality_reinspect',
    columns: [{
        name: 'id', type: 'varchar(255)'
    }, {
        name: 'update_time', type: 'int'
    }, {
        name: 'has_read', type: 'bit(1)'
    }, {
        name: 'reading_time', type: 'int'
    }, {
        name: 'checkquality_problem_id', type: 'varchar(255)'
    }, {
        name: 'reinspect', type: 'varchar(255)'
    }]
});
tables.list.push({
    name: 'tb_checkquality_cc',
    columns: [{
        name: 'id', type: 'varchar(255)'
    }, {
        name: 'update_time', type: 'int'
    }, {
        name: 'checkquality_problem_id', type: 'varchar(255)'
    }, {
        name: 'cc', type: 'varchar(255)'
    }, {
        name: 'type', type: 'varchar(255)'
    }, {
        name: 'reading_time', type: 'int'
    }, {
        name: 'reading_status', type: 'varchar(255)'
    }]
});
tables.list.push({
    name: 'tb_quality_measure_checkquality_problem',
    columns: [{
        name: 'id', type: 'varchar(255)'
    }, {
        name: 'update_time', type: 'int'
    }, {
        name: 'batch_type', type: 'varchar(20)'
    }, {
        name: 'drawing_file', type: 'varchar(1024)'
    }, {
        name: 'image_file', type: 'varchar(1024)'
    }, {
        name: 'measure_points', type: 'int'
    }, {
        name: 'problem_points', type: 'int'
    }, {
        name: 'problem_values', type: 'varchar(4096)'
    }, {
        name: 'regist_date', type: 'int'
    }, {
        name: 'remark', type: 'varchar(255)'
    }, {
        name: 'repair_date', type: 'int'
    }, {
        name: 'repair_image_file', type: 'varchar(1024)'
    }, {
        name: 'repair_remark', type: 'varchar(255)'
    }, {
        name: 'sent_back_date', type: 'int'
    }, {
        name: 'sent_back_image_file', type: 'varchar(1024)'
    }, {
        name: 'sent_back_remark', type: 'varchar(255)'
    }, {
        name: 'sent_back_times', type: 'int'
    }, {
        name: 'small_image_file', type: 'varchar(1024)'
    }, {
        name: 'small_repair_image_file', type: 'varchar(1024)'
    }, {
        name: 'small_sent_back_image_file', type: 'varchar(1024)'
    }, {
        name: 'standard_min', type: 'double'
    }, {
        name: 'standard_max', type: 'double'
    }, {
        name: 'status', type: 'varchar(255)'
    }, {
        name: 'building_id', type: 'varchar(255)'
    }, {
        name: 'floor_id', type: 'varchar(255)'
    }, {
        name: 'room_id', type: 'varchar(255)'
    }, {
        name: 'item_id', type: 'varchar(255)'
    }, {
        name: 'regist_by', type: 'varchar(255)'
    }, {
        name: 'repair_by', type: 'varchar(255)'
    }, {
        name: 'unit_id', type: 'varchar(255)'
    }, {
        name: 'app_permission', type: 'varchar(255)'
    }, {
    	name: 'ccs', type: 'varchar(10240)'
    }]
});
tables.list.push({
    name: 'tb_quality_measure_checkquality_repair',
    columns: [{
        name: 'id', type: 'varchar(255)'
    }, {
        name: 'update_time', type: 'int'
    }, {
        name: 'has_read', type: 'bit(1)'
    }, {
        name: 'reading_time', type: 'int'
    }, {
        name: 'quality_measure_checkquality_problem_id', type: 'varchar(255)'
    }, {
        name: 'child_quality_measure_checkquality_repair_id', type: 'varchar(255)'
    }, {
        name: 'parent_quality_measure_checkquality_repair_id', type: 'varchar(255)'
    }, {
        name: 'repair', type: 'varchar(255)'
    }]
});
tables.list.push({
    name: 'tb_quality_measure_checkquality_cc',
    columns: [{
        name: 'id', type: 'varchar(255)'
    }, {
        name: 'update_time', type: 'int'
    }, {
        name: 'quality_measure_checkquality_problem_id', type: 'varchar(255)'
    }, {
        name: 'cc', type: 'varchar(255)'
    }, {
        name: 'type', type: 'varchar(255)'
    }, {
        name: 'reading_time', type: 'int'
    }, {
        name: 'reading_status', type: 'varchar(255)'
    }]
});
tables.list.push({
    name: 'tmp_locale_check_checkquality_problem_regist',
    columns: [{
        name: 'batch_id', type: 'varchar(100)'
    }, {
        name: 'building_id', type: 'varchar(100)',
    }, {
        name: 'unit_id', type: 'varchar(100)',
    }, {
        name: 'floor_id', type: 'varchar(100)',
    }, {
        name: 'house_id', type: 'varchar(100)',
    }, {
        name: 'place', type: 'varchar(100)',
    }, {
        name: 'problem_class_id', type: 'varchar(100)',
    }, {
        name: 'problem_id', type: 'varchar(100)',
    }, {
        name: 'regist', type: 'varchar(100)',
    }, {
        name: 'remark', type: 'varchar(100)',
    }, {
        name: 'emergency_degree', type: 'varchar(100)',
    }, {
        name: 'repair_deadline', type: 'integer',
    }, {
        name: 'contractor_id', type: 'varchar(100)',
    }, {
        name: 'repair', type: 'varchar(10240)',
    }, {
        name: 'reinspects', type: 'varchar(10240)',
    }, {
        name: 'ccs', type: 'varchar(10240)',
    }, {
        name: 'photos', type: 'varchar(10240)',
    }, {
        name: 'problem_values', type: 'varchar(10240)'
    }]
});
tables.list.push({
    name: 'tmp_quality_measure_checkquality_problem_regist',
    columns: [{
        name: 'building_id', type: 'varchar(100)',
    }, {
        name: 'unit_id', type: 'varchar(100)',
    }, {
        name: 'floor_id', type: 'varchar(100)',
    }, {
        name: 'house_id', type: 'varchar(100)',
    }, {
        name: 'item_id', type: 'varchar(100)',
    }, {
        name: 'regist', type: 'varchar(100)',
    }, {
        name: 'repair', type: 'varchar(1024)',
    }, {
        name: 'ccs', type: 'varchar(10240)',
    }, {
        name: 'photos', type: 'varchar(10240)',
    }, {
        name: 'measure_points', type: 'int',
    }, {
        name: 'problem_values', type: 'varchar(10240)'
    }, {
        name: 'app_permission', type: 'varchar(10240)'
    }]
});
tables.list.push({
    name: 'tmp_quality_measure_checkquality_problem_modify',
    columns: [{
        name: 'quality_measure_checkquality_problem_id', type: 'varchar(100)',
    }, {
        name: 'regist', type: 'varchar(100)',
    }, {
        name: 'repair', type: 'varchar(1024)',
    }, {
        name: 'ccs', type: 'varchar(10240)',
    }, {
        name: 'photos', type: 'varchar(10240)',
    }, {
        name: 'measure_points', type: 'int',
    }, {
        name: 'problem_values', type: 'varchar(10240)'
    }]
});
tables.list.push({
    name: 'tmp_unprocessed_problem_local_check_unprocessed',
    columns: [{
        name: 'id', type: 'varchar(255)'
    }, {
        name: 'update_time', type: 'int'
    }, {
        name: 'batch_id', type: 'varchar(255)'
    }, {
        name: 'batch_type', type: 'varchar(20)'
    }, {
        name: 'item_id', type: 'varchar(255)'
    }, {
        name: 'contractor_id', type: 'varchar(255)'
    }, {
        name: 'desc_id', type: 'varchar(255)'
    }, {
        name: 'remark', type: 'varchar(255)'
    }, {
        name: 'image_file', type: 'varchar(2048)'
    }, {
        name: 'small_image_file', type: 'varchar(1024)'
    }, {
        name: 'building_id', type: 'varchar(255)'
    }, {
        name: 'unit_id', type: 'varchar(255)'
    }, {
        name: 'floor_id', type: 'varchar(255)'
    }, {
        name: 'room_id', type: 'varchar(255)'
    }, {
        name: 'house_type_id', type: 'varchar(255)'
    }, {
        name: 'place', type: 'varchar(255)'
    }, {
        name: 'status', type: 'varchar(255)'
    }, {
        name: 'emergency_degree', type: 'varchar(255)'
    }, {
        name: 'regist_by', type: 'varchar(255)'
    }, {
        name: 'regist_date', type: 'int'
    }, {
        name: 'repair_by', type: 'varchar(255)'
    }, {
        name: 'close_date', type: 'int'
    }, {
        name: 'close_reason', type: 'varchar(255)'
    }, {
        name: 'drawing_file', type: 'varchar(1024)'
    }, {
        name: 'problem_values', type: 'varchar(1024)'
    }, {
        name: 'reinspect_completed_date', type: 'int'
    }, {
        name: 'reinspect_image_file', type: 'varchar(1024)'
    }, {
        name: 'reinspect_remark', type: 'varchar(255)'
    }, {
        name: 'repair_date', type: 'int'
    }, {
        name: 'repair_deadline', type: 'int'
    }, {
        name: 'repair_image_file', type: 'varchar(2048)'
    }, {
        name: 'repair_remark', type: 'varchar(255)'
    }, {
        name: 'sent_back_date', type: 'int'
    }, {
        name: 'sent_back_image_file', type: 'varchar(1024)'
    }, {
        name: 'sent_back_remark', type: 'varchar(255)'
    }, {
        name: 'sent_back_times', type: 'int'
    }, {
        name: 'small_reinspect_image_file', type: 'varchar(1024)'
    }, {
        name: 'small_repair_image_file', type: 'varchar(1024)'
    }, {
        name: 'small_sent_back_image_file', type: 'varchar(1024)'
    }, {
        name: 'reinspect_by', type: 'varchar(255)'
    }, {
        name: 'sent_back_by', type: 'varchar(255)'
    }, {
        name: 'ccs', type: 'varchar(255)'
    }, {
        name: 'building_name', type: 'varchar(255)'
    }, {
        name: 'unit_name', type: 'varchar(255)'
    }, {
        name: 'floor_name', type: 'varchar(255)'
    }, {
        name: 'room_name', type: 'varchar(255)'
    }, {
    	name: 'bid_section_id', type: 'varchar(255)'
    }]
});
tables.list.push({
    name: 'tmp_unprocessed_problem_local_check_processed',
    columns: [{
        name: 'id', type: 'varchar(255)'
    }, {
        name: 'update_time', type: 'int'
    }, {
        name: 'batch_id', type: 'varchar(255)'
    }, {
        name: 'batch_type', type: 'varchar(20)'
    }, {
        name: 'item_id', type: 'varchar(255)'
    }, {
        name: 'contractor_id', type: 'varchar(255)'
    }, {
        name: 'desc_id', type: 'varchar(255)'
    }, {
        name: 'remark', type: 'varchar(255)'
    }, {
        name: 'image_file', type: 'varchar(2048)'
    }, {
        name: 'small_image_file', type: 'varchar(1024)'
    }, {
        name: 'building_id', type: 'varchar(255)'
    }, {
        name: 'unit_id', type: 'varchar(255)'
    }, {
        name: 'floor_id', type: 'varchar(255)'
    }, {
        name: 'room_id', type: 'varchar(255)'
    }, {
        name: 'house_type_id', type: 'varchar(255)'
    }, {
        name: 'place', type: 'varchar(255)'
    }, {
        name: 'status', type: 'varchar(255)'
    }, {
        name: 'emergency_degree', type: 'varchar(255)'
    }, {
        name: 'regist_by', type: 'varchar(255)'
    }, {
        name: 'regist_date', type: 'int'
    }, {
        name: 'repair_by', type: 'varchar(255)'
    }, {
        name: 'close_date', type: 'int'
    }, {
        name: 'close_reason', type: 'varchar(255)'
    }, {
        name: 'drawing_file', type: 'varchar(1024)'
    }, {
        name: 'problem_values', type: 'varchar(1024)'
    }, {
        name: 'reinspect_completed_date', type: 'int'
    }, {
        name: 'reinspect_image_file', type: 'varchar(1024)'
    }, {
        name: 'reinspect_remark', type: 'varchar(255)'
    }, {
        name: 'repair_date', type: 'int'
    }, {
        name: 'repair_deadline', type: 'int'
    }, {
        name: 'repair_image_file', type: 'varchar(2048)'
    }, {
        name: 'repair_remark', type: 'varchar(255)'
    }, {
        name: 'sent_back_date', type: 'int'
    }, {
        name: 'sent_back_image_file', type: 'varchar(1024)'
    }, {
        name: 'sent_back_remark', type: 'varchar(255)'
    }, {
        name: 'sent_back_times', type: 'int'
    }, {
        name: 'small_reinspect_image_file', type: 'varchar(1024)'
    }, {
        name: 'small_repair_image_file', type: 'varchar(1024)'
    }, {
        name: 'small_sent_back_image_file', type: 'varchar(1024)'
    }, {
        name: 'reinspect_by', type: 'varchar(255)'
    }, {
        name: 'sent_back_by', type: 'varchar(255)'
    }, {
        name: 'ccs', type: 'varchar(255)'
    }, {
        name: 'building_name', type: 'varchar(255)'
    }, {
        name: 'unit_name', type: 'varchar(255)'
    }, {
        name: 'floor_name', type: 'varchar(255)'
    }, {
        name: 'room_name', type: 'varchar(255)'
    }, {
    	name: 'bid_section_id', type: 'varchar(255)'
    }]
});
tables.list.push({
    name: 'tmp_unprocessed_problem_quality_measure_unprocessed',
    columns: [{
        name: 'id', type: 'varchar(255)'
    }, {
        name: 'update_time', type: 'int'
    }, {
        name: 'batch_type', type: 'varchar(20)'
    }, {
        name: 'drawing_file', type: 'varchar(1024)'
    }, {
        name: 'image_file', type: 'varchar(1024)'
    }, {
        name: 'measure_points', type: 'int'
    }, {
        name: 'problem_points', type: 'int'
    }, {
        name: 'problem_values', type: 'varchar(4096)'
    }, {
        name: 'regist_date', type: 'int'
    }, {
        name: 'remark', type: 'varchar(255)'
    }, {
        name: 'repair_date', type: 'int'
    }, {
        name: 'repair_image_file', type: 'varchar(1024)'
    }, {
        name: 'repair_remark', type: 'varchar(255)'
    }, {
        name: 'sent_back_date', type: 'int'
    }, {
        name: 'sent_back_image_file', type: 'varchar(1024)'
    }, {
        name: 'sent_back_remark', type: 'varchar(255)'
    }, {
        name: 'sent_back_times', type: 'int'
    }, {
        name: 'small_image_file', type: 'varchar(1024)'
    }, {
        name: 'small_repair_image_file', type: 'varchar(1024)'
    }, {
        name: 'small_sent_back_image_file', type: 'varchar(1024)'
    }, {
        name: 'standard_min', type: 'double'
    }, {
        name: 'standard_max', type: 'double'
    }, {
        name: 'status', type: 'varchar(255)'
    }, {
        name: 'building_id', type: 'varchar(255)'
    }, {
        name: 'floor_id', type: 'varchar(255)'
    }, {
        name: 'room_id', type: 'varchar(255)'
    }, {
        name: 'item_id', type: 'varchar(255)'
    }, {
        name: 'regist_by', type: 'varchar(255)'
    }, {
        name: 'repair_by', type: 'varchar(255)'
    }, {
        name: 'unit_id', type: 'varchar(255)'
    }, {
        name: 'app_permission', type: 'varchar(255)'
    }, {
    	name: 'ccs', type: 'varchar(10240)'
    }, {
        name: 'building_name', type: 'varchar(255)'
    }, {
        name: 'floor_name', type: 'varchar(255)'
    }, {
        name: 'room_name', type: 'varchar(255)'
    }, {
        name: 'unit_name', type: 'varchar(255)'
    }, {
    	name: 'bid_section_id', type: 'varchar(255)'
    }]
});
tables.list.push({
    name: 'tmp_unprocessed_problem_quality_measure_processed',
    columns: [{
        name: 'id', type: 'varchar(255)'
    }, {
        name: 'update_time', type: 'int'
    }, {
        name: 'batch_type', type: 'varchar(20)'
    }, {
        name: 'drawing_file', type: 'varchar(1024)'
    }, {
        name: 'image_file', type: 'varchar(1024)'
    }, {
        name: 'measure_points', type: 'int'
    }, {
        name: 'problem_points', type: 'int'
    }, {
        name: 'problem_values', type: 'varchar(4096)'
    }, {
        name: 'regist_date', type: 'int'
    }, {
        name: 'remark', type: 'varchar(255)'
    }, {
        name: 'repair_date', type: 'int'
    }, {
        name: 'repair_image_file', type: 'varchar(1024)'
    }, {
        name: 'repair_remark', type: 'varchar(255)'
    }, {
        name: 'sent_back_date', type: 'int'
    }, {
        name: 'sent_back_image_file', type: 'varchar(1024)'
    }, {
        name: 'sent_back_remark', type: 'varchar(255)'
    }, {
        name: 'sent_back_times', type: 'int'
    }, {
        name: 'small_image_file', type: 'varchar(1024)'
    }, {
        name: 'small_repair_image_file', type: 'varchar(1024)'
    }, {
        name: 'small_sent_back_image_file', type: 'varchar(1024)'
    }, {
        name: 'standard_min', type: 'double'
    }, {
        name: 'standard_max', type: 'double'
    }, {
        name: 'status', type: 'varchar(255)'
    }, {
        name: 'building_id', type: 'varchar(255)'
    }, {
        name: 'floor_id', type: 'varchar(255)'
    }, {
        name: 'room_id', type: 'varchar(255)'
    }, {
        name: 'item_id', type: 'varchar(255)'
    }, {
        name: 'regist_by', type: 'varchar(255)'
    }, {
        name: 'repair_by', type: 'varchar(255)'
    }, {
        name: 'unit_id', type: 'varchar(255)'
    }, {
        name: 'app_permission', type: 'varchar(255)'
    }, {
    	name: 'ccs', type: 'varchar(10240)'
    }, {
        name: 'building_name', type: 'varchar(255)'
    }, {
        name: 'floor_name', type: 'varchar(255)'
    }, {
        name: 'room_name', type: 'varchar(255)'
    }, {
        name: 'unit_name', type: 'varchar(255)'
    }, {
    	name: 'bid_section_id', type: 'varchar(255)'
    }]
});
tables.list.push({
    name: 'offline_local_check_repair',
    columns: [{
        name: 'checkquality_problem_id', type: 'varchar(255)'
    }, {
        name: 'update_time', type: 'int'
    }, {
        name: 'offline_repair_image_file', type: 'varchar(1024)'
    }, {
        name: 'offline_repair_remark', type: 'varchar(255)'
    }]
});
tables.list.push({
    name: 'offline_local_check_reinspect',
    columns: [{
        name: 'checkquality_problem_id', type: 'varchar(255)'
    }, {
        name: 'update_time', type: 'int'
    }, {
        name: 'offline_reinspect_image_file', type: 'varchar(1024)'
    }, {
        name: 'offline_reinspect_remark', type: 'varchar(255)'
    }]
});
tables.list.push({
    name: 'offline_local_check_sendBack',
    columns: [{
        name: 'problem_id', type: 'varchar(255)'
    }, {
        name: 'update_time', type: 'int'
    }, {
        name: 'offline_sendBack_image_file', type: 'varchar(1024)'
    }, {
        name: 'offline_sendBack_remark', type: 'varchar(255)'
    }, {
    	name: 'back_reason_value', type: 'varchar(255)'
    }]
});
tables.list.push({
    name: 'offline_quality_measure_sendBack',
    columns: [{
        name: 'problem_id', type: 'varchar(255)'
    }, {
        name: 'update_time', type: 'int'
    }, {
        name: 'offline_sendBack_image_file', type: 'varchar(1024)'
    }, {
        name: 'offline_sendBack_remark', type: 'varchar(255)'
    }, {
    	name: 'back_reason_value', type: 'varchar(255)'
    }]
});
tables.list.push({
    name: 'offline_quality_measure_repair',
    columns: [{
        name: 'problem_id', type: 'varchar(255)'
    }, {
        name: 'update_time', type: 'int'
    }, {
        name: 'offline_repair_image_file', type: 'varchar(1024)'
    }, {
        name: 'offline_repair_remark', type: 'varchar(255)'
    }]
});
tables.list.push({
    name: 'tmp_quality_measure_rate',
    columns: [{
        name: 'item_id', type: 'varchar(255)',
    }, {
        name: 'unit_id', type: 'varchar(255)',
    }, {
        name: 'rate', type: 'double',
    }]
});
for(let i = 0; i < tables.list.length; i++) {
	let table = tables.list[i];
	tables.map[table.name] = table;
}
export default tables;