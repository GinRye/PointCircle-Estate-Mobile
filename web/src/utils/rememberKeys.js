var keys = {
	projectIdKey: "projectIdKey",
	localeCheckBatchIdKey: 'localeCheckBatchIdKey',
	localeCheckProblemRegistKey: 'localeCheckProblemRegistKey',
	qualityMeasureUnitIdKey: 'qualityMeasureUnitId',
	qualityMeasureProblemClassIdKey: 'qualityMeasureProblemClassId'
}

export default keys;