(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
  typeof define === 'function' && define.amd ? define(factory) :
  (global.Common = factory());
}(this, (function () { 'use strict';

    var Common = {};
    
    Common.directory = function() {
    	return {
	    	base: window.cordova.platformId == 'android' ? 
	    		window.cordova.file.externalApplicationStorageDirectory : 
	    		window.cordova.file.dataDirectory,
	     	houseType: "houseType/",
	    	temp: "temp/",
	    	localeCheck: "localeCheck/",
	    	qualityMeasure: "qualityMeasure/"
    	}
    };
    
    Common.isOnline = function() {
        var networkState = window.parent.navigator.connection.type || window.navigator.connection.type;
        if(networkState == 'wifi' || networkState == '4g') {
            return true;
        } else {
            return false;
        }
    }
    
    Common.uuid = function() {
        var s = [];
        var hexDigits = "0123456789abcdef";
        for (var i = 0; i < 36; i++) {
            s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
        }
        s[14] = "4";
        s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1);
        s[8] = s[13] = s[18] = s[23] = "-";
     
        var uuid = s.join("");
        return uuid;
    }
    
    Common.executeSql = function(sql, values) {
    	var promise = new Promise(function(resolve, reject) {
    		window.db.transaction(function(tx) {
    			tx.executeSql(sql, values, function(tx, rs) {
    				resolve({tx, rs});
    			});
    		}, function(error) {
    			reject(error);
    		});
    	});
    	return promise;
    }
    
    Common.sqlBatch = function(sqlBatches) {
    	var promise = new Promise(function(resolve, reject) {
    		window.db.sqlBatch(sqlBatches, function() {
    			resolve();
    		}, function(error) {
    			reject(error);
    		});
    	});
    	return promise;
    }
    
    Common.strToJson = function(str) {
        try {
            var str = str.replace(/\\/g, "");
            var obj = JSON.parse(str);
        } catch(e) {
            return null;
        }
        return obj;
    }
    
    // 下划线转换驼峰
    Common.toHump = function(name) {
        return name.replace(/\_(\w)/g, function(all, letter){
            return letter.toUpperCase();
        });
    }
    // 驼峰转换下划线
    Common.toLine =function(name) {
      return name.replace(/([A-Z])/g,"_$1").toLowerCase();
    }
    
    return Common;
})));
