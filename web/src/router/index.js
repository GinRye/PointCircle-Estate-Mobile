// 带query参数的页面，不能使用keepAlive
// 不带keepAlive参数，页面深度不能如果超过两层，如果超过将会销毁，导致页面保存数据丢失。

import Login from '@/views/login';
import LoginForgetPassword from '@/views/login/forgetPassword';
import Home from '@/views/home';
import Workbench from '@/views/home/workbench';
import WorkbenchLocaleCheck from '@/views/home/workbench/localeCheck';
import WorkbenchLocaleCheckBatchList from '@/views/home/workbench/localeCheck/batchList';
import WorkbenchLocaleCheckBatchDetail from '@/views/home/workbench/localeCheck/batchDetail';
import WorkbenchLocaleCheckBatchModify from '@/views/home/workbench/localeCheck/batchModify';
import WorkbenchLocaleCheckProblemRegist from '@/views/home/workbench/localeCheck/problemRegist';
import WorkbenchLocaleCheckProblemRegistSubmit from '@/views/home/workbench/localeCheck/problemRegistSubmit';
import WorkbenchQualityMeasure from '@/views/home/workbench/qualityMeasure';
import Todo from '@/views/home/todo';
import TodoLocaleCheck from '@/views/home/todo/localeCheck';
import TodoQualityMeasure from '@/views/home/todo/qualityMeasure';
import Settings from '@/views/home/settings';
import SettingsResetPassword from '@/views/home/settings/resetPassword';
import HousePoint from '@/views/common/HousePoint';
import LocaleCheckComplete from '@/views/common/LocaleCheckComplete';
import LocaleCheckClose from '@/views/common/LocaleCheckClose';
import LocaleCheckRepair from '@/views/common/LocaleCheckRepair';
import LocaleCheckReinspect from '@/views/common/LocaleCheckReinspect';
import WorkbenchQualityMeasureProblemSummary from '@/views/home/workbench/qualityMeasure/ProblemSummary';
import WorkbenchQualityMeasureHouseDetail from "@/views/home/workbench/qualityMeasure/HouseDetail";
import ProblemList from '@/views/common/ProblemList';
import ChangeDate from '@/views/home/todo/common/ChangeDate';
import QualityMeasureProblemModify from "@/components/QualityMeasureProblemModify";
import QualityMeasureProblemReinspect from "@/components/QualityMeasureProblemReinspect";
import QualityMeasureProblemComplete from "@/views/home/todo/qualityMeasure/QualityMeasureProblemComplete";
import QualityMeasureProblemRepair from "@/views/home/todo/qualityMeasure/QualityMeasureProblemRepair";
import SendBack from "@/components/SendBack";

export default [{
	path: '/Login',
	component: Login,
	name: 'Login',
	transition: 'f7-parallax'
}, {
    path: '/Login/ForgetPassword',
    component: LoginForgetPassword,
	name: 'LoginForgetPassword',
	transition: 'f7-parallax'
}, {
	path: '/Home',
	component: Home,
	name: 'Home',
	transition: 'f7-parallax'
}, {
	path: '/Workbench',
	component: Workbench,
	name: 'Workbench',
	transition: 'f7-parallax'
}, {
	path: '/Workbench/LocaleCheck',
	component: WorkbenchLocaleCheck,
	name: 'WorkbenchLocaleCheck',
	transition: 'f7-parallax',
    keepAlive: true
}, {
	path: '/Workbench/LocaleCheck/BatchList',
	component: WorkbenchLocaleCheckBatchList,
	name: 'WorkbenchLocaleCheckBatchList',
	transition: 'f7-parallax',
    keepAlive: true
}, {
	path: '/Workbench/LocaleCheck/BatchDetail',
	component: WorkbenchLocaleCheckBatchDetail,
	name: 'WorkbenchLocaleCheckBatchDetail',
	transition: 'f7-parallax'
}, {
	path: '/Workbench/LocaleCheck/BatchModify',
	component: WorkbenchLocaleCheckBatchModify,
	name: 'WorkbenchLocaleCheckBatchModify',
	transition: 'f7-parallax'
}, {
	path: '/Workbench/LocaleCheck/ProblemRegist',
	component: WorkbenchLocaleCheckProblemRegist,
	name: 'WorkbenchLocaleCheckProblemRegist',
	transition: 'f7-parallax'
}, {
	path: '/Workbench/LocaleCheck/ProblemRegistSubmit',
	component: WorkbenchLocaleCheckProblemRegistSubmit,
	name: 'WorkbenchLocaleCheckProblemRegistSubmit',
	transition: 'f7-parallax'
}, {
	path: '/Workbench/QualityMeasure',
	component: WorkbenchQualityMeasure,
	name: 'WorkbenchQualityMeasure',
	transition: 'f7-parallax',
	keepAlive: true
}, {
	path: '/Todo',
	component: Todo,
	name: 'Todo',
	transition: 'f7-parallax',
	keepAlive: true
}, {
	path: '/Todo/LocaleCheck',
	component: TodoLocaleCheck,
	name: 'TodoLocaleCheck',
	transition: 'f7-parallax',
	keepAlive: true
}, {
	path: '/Todo/QualityMeasure',
	component: TodoQualityMeasure,
	name: 'TodoQualityMeasure',
	transition: 'f7-parallax',
	keepAlive: true
},{
    path: '/Settings',
    component: Settings,
    name: 'Settings',
    transition: 'f7-parallax'
}, {
    path: '/Settings/ResetPassword',
    component: SettingsResetPassword,
    name: 'SettingsResetPassword',
    transition: 'f7-parallax'
}, {
	path: '/HousePoint',
	component: HousePoint,
	name: 'HousePoint',
	transition: 'f7-parallax'
}, {
	path: '/LocaleCheckComplete',
	component: LocaleCheckComplete,
	name: 'LocaleCheckComplete',
	transition: 'f7-parallax'
}, {
	path: '/LocaleCheckClose',
	component: LocaleCheckClose,
	name: 'LocaleCheckClose',
	transition: 'f7-parallax'
}, {
	path: '/LocaleCheckRepair',
	component: LocaleCheckRepair,
	name: 'LocaleCheckRepair',
	transition: 'f7-parallax'
}, {
	path: '/LocaleCheckReinspect',
	component: LocaleCheckReinspect,
	name: 'LocaleCheckReinspect',
	transition: 'f7-parallax'
}, {
	path: '/WorkbenchQualityMeasureProblemSummary',
	component: WorkbenchQualityMeasureProblemSummary,
	name: 'WorkbenchQualityMeasureProblemSummary',
	transition: 'f7-parallax'
}, {
	path: '/WorkbenchQualityMeasureHouseDetail',
	component: WorkbenchQualityMeasureHouseDetail,
	name: 'WorkbenchQualityMeasureHouseDetail',
	transition: 'f7-parallax'
},{
    path: '/ProblemList',
    component: ProblemList,
    name: 'ProblemList',
    transition: 'f7-parallax'
},{
    path: '/ChangeDate',
    component: ChangeDate,
    name: 'ChangeDate',
    transition: 'f7-parallax',
    keepAlive: true
}, {
	path: '/QualityMeasureProblemModify',
	component: QualityMeasureProblemModify,
	name: 'QualityMeasureProblemModify',
	transition: 'f7-parallax'
}, {
	path: '/QualityMeasureProblemReinspect',
	component: QualityMeasureProblemReinspect,
	name: 'QualityMeasureProblemReinspect',
	transition: 'f7-parallax'
}, {
	path: '/QualityMeasureProblemComplete',
	component: QualityMeasureProblemComplete,
	name: 'QualityMeasureProblemComplete',
	transition: 'f7-parallax'
}, {
	path: '/QualityMeasureProblemRepair',
	component: QualityMeasureProblemRepair,
	name: 'QualityMeasureProblemRepair',
	transition: 'f7-parallax'
}, {
	path: '/SendBack',
	component: SendBack,
	name: 'SendBack',
	transition: 'f7-parallax'
}];
