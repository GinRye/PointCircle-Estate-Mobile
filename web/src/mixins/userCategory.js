import store from '@/store';
import { getStore, setStore, clearStore } from '@/utils/storage';
import keys from '@/utils/rememberKeys';

export default {
	computed: {
		contractors() {
			return userCategoryIds('施工单位');
		},
		supervisors() {
			return userCategoryIds('监理单位');
		},
		constructors() {
			return userCategoryIds('建设单位');
		}
	}
}

function userCategoryIds(category) {
	let set = new Set();
	let projectId = getStore(keys.projectIdKey);
	let userIds = store.state.global.userProjectRel.projectMap[projectId] || [];
	for(let i = 0; i < userIds.length; i++) {
		let userId = userIds[i];
		let user = store.state.global.userMap[userId];
		if(user.app_permission == category) {
			set.add(userId);
		}
	}
	let ids = [];
	let itr = set.keys();
	do {
		let value = itr.next().value;
		if(value == null) {
			break;
		}
		ids.push(value);
	} while(true);
	return ids;
}