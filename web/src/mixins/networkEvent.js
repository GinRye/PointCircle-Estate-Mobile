import eventBus from '@/utils/eventBus.js';
import Common from '@/utils/Common.js';

export default {
	data() {
		return {
			isOnline: Common.isOnline()
		}
	},
	beforeCreate() {
	    var self = this;
	    this.networkChanged = function() {
			self.isOnline = Common.isOnline();
		}
		eventBus.$on('networkChanged', this.networkChanged);
	},
	beforeDestroy() {
		eventBus.$off('networkChanged', this.networkChanged);
	}
}