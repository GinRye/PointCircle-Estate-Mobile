export default {
	data() {
		return {
			countDown: 0
		}
	},
	methods: {
		toggleCheckCode() {
			this.countDown = 60;
			var self = this;
			var intervalId = setInterval(function() {
				self.countDown--;
				if(self.countDown == 0) {
					clearInterval(intervalId);
				}
			}, 1000);
		}
	},
	computed: {
		showCountDown() {
			return this.countDown + '秒';
		}
	}
}