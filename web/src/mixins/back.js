export default {
	data() {
		return {
			isAlert: false,
			backMessage: ''
		}
	},
	methods: {
		back() {
			var self = this;
			self.$f7.preloader.hide();
			if(this.isAlert) {
	            this.$f7.dialog.confirm("", this.backMessage, function() {
	                self.$f7router.back();
	            });
			} else {
				this.$f7router.back();
			}
		}
	},
	mounted() {
		var self = this;
		Dom7(this.$el).on('page:beforein', function() {
			for(var i = 0; i < self.$f7.views.length; i++) {
				var view = self.$f7.views[i];
				if(view.name == window.currentViewName) {
					view.$vm = self;
					break;
				}
			}
		});
	}
}