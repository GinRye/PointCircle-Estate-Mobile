import Vue from 'vue';
import '@/assets/framework7/css/framework7.css';
import '@/assets/framework7/css/icons.css';
import 'material-design-icons-iconfont/dist/material-design-icons.css';
import App from '@/App.vue';

import Framework7 from '@/assets/framework7/js/framework7.js';
import Framework7Vue from '@/assets/framework7/js/framework7-vue.js';
Framework7.use(Framework7Vue);

import Common from '@/utils/Common.js';
import eventBus from '@/utils/eventBus.js';
import request from '@/utils/request.js';
import axios from 'axios';

var version = 2;

document.addEventListener("deviceready", function() {
	window.db = window.sqlitePlugin.openDatabase({
        name: 'pointcircle-estate.db',
        location: 'default'
    }, db => {
		new Vue({
			el: '#app',
			render: (h) => h(App)
		});
    }, error => {
    	console.error(error);
    });
	window.resolveLocalFileSystemURL(Common.directory().base, function(fs) {
	    for(let key in Common.directory()) {
	        let dirName = Common.directory()[key];
	        (function(dirName) {
	            fs.getDirectory(dirName, {create: true}, function(dirEntry) {
	                if(dirName == Common.directory().temp) {
	                    let dirReader = dirEntry.createReader();
	                    dirReader.readEntries(entries => {
	                        entries.forEach(entry => {
	                            entry.remove();
	                        });
	                    });
	                }
	            });
	        })(dirName);
	    }
	});
});

document.addEventListener('offline', function() {
    eventBus.$emit('networkChanged');
});
document.addEventListener('online', function() {
    eventBus.$emit('networkChanged');
});
document.addEventListener('backbutton', function() {
    for(var i = 0; i < vm.$f7.views.length; i++) {
        var view = vm.$f7.views[i];
        if(view.name == window.currentViewName) {
            if(view.$vm != null && view.$vm.back != null) {
                view.$vm.back();
            } else {
                view.router.back();
            }
            break;
        }
    }
});

if(Common.isOnline()) {
	axios({
		url: 'http://estate-oss.point-circle.com/config.json',
		method: 'get'
	}).then(response => {
		if(response.data.appVersion > version) {
			vm.$f7.dialog.alert('', '请升级到最新版本', function() {
				cordova.InAppBrowser.open('http://gz.polygz.com:18080/img/estatemanage-V1-3.2.apk', '_system');
				navigator.app.exitApp();
			});
		}
		const debug = process.env.NODE_ENV !== 'production'
		if(!debug) {
			codePush.checkForUpdate(function(update) {
			    if(!update) {
			        console.log("已更新到最新版本...");
			    } else {
			    	var dialog = vm.$f7.dialog.progress('版本更新中', 0);
			    	window.codePush.sync(syncStatus, null, downloadProgress);
			    	function syncStatus(status) {
			    		switch(status) {
			    			case SyncStatus.DOWNLOADING_PACKAGE:
			    				console.log('下载更新中...');
			    	            break;
			    	        case SyncStatus.INSTALLING_UPDATE:
			    	        	console.log('安装更新中...');
			    	            break;
			    	        case SyncStatus.UPDATE_INSTALLED:
			    	        	console.log('安装完成...')
			    	        	dialog.close();
			    	    }
			    	}
			    	function downloadProgress(downloadProgress) {
			    		if(downloadProgress) {
			    			var percent = parseInt(downloadProgress.receivedBytes / downloadProgress.totalBytes * 100);
				    		dialog.setProgress(percent);
				    		dialog.setText('已完成：' + percent + '%');
			    		}
			    	}
			    }
			});
		}
	}).catch(error => {
		console.error(error);
	});
}
