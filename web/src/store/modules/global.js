import Common from '@/utils/Common.js';
import { getStore, setStore, clearStore } from '@/utils/storage';
import keys from '@/utils/rememberKeys';

const global = {
	state: {
		userMap: {},
		projectMap: {},
		bidSectionMap: {},
		contractorTypeMap: {},
		problemClassMap: {},
		problemMap: {},
		checkItemGuidelineMap: {},
		houseTypeMap: {},
		bidSectionCheckItemRel: {
			bidSectionMap: {},
			checkItemMap: {}
		},
		bidSectionContractorRel: {
			bidSectionMap: {},
			contractorMap: {}
		},
		buildingMap: {},
		floorMap: {},
		unitMap: {},
		houseMap: {
			floorUnitMap: {}
		},
		userProjectRel: {
			userMap: {},
			projectMap: {}
		},
		unitCheckItemRateMap: {},
		draftMap: {},
		modifyMap: {}
	},
	actions: {
		load(context, types) {
			window.db.transaction(tx => {
				types.forEach(type => {
					let tableName = type.tableName;
					let mapName = type.mapName;
					let sql = "select * from " + tableName;
					tx.executeSql(sql, [], (tx, rs) => {
						var map = {};
						for(let i = 0; i < rs.rows.length; i++) {
							let row = rs.rows.item(i);
							map[row.id] = row;
						}
						context.state[mapName] = map;
						console.log('store load finish: ' + mapName);
					});
				});
			}, error => {
				console.error(error);
			});
		},
		user(context) {
			loadMap(context, 'tb_user', 'userMap');
		},
		project(context) {
			loadMap(context, 'tb_project', 'projectMap');
		},
		bidSection(context) {
			loadMap(context, 'tb_bid_section', 'bidSectionMap');
		},
		contractorType(context) {
			loadMap(context, 'tb_contractor_type', 'contractorTypeMap');
		},
		problemClass(context) {
			loadMap(context, 'tb_problem_class', 'problemClassMap');
		},
		problem(context) {
			loadMap(context, 'tb_problem', 'problemMap');
		},
		checkItemGuideline(context) {
			var sql = "select * from tb_check_item_guideline ";
			window.db.executeSql(sql, [], (rs) => {
	        	for(var i = 0; i < rs.rows.length; i++) {
	        		var row = rs.rows.item(i);
	        		context.state.checkItemGuidelineMap[row.problem_class_id] = row;
	        	}
	        	console.log('store load finish: checkItemGuidelineMap');
	        }, error => {
				console.error(error);
			});
		},
		houseType(context) {
			loadMap(context, 'tb_house_type', 'houseTypeMap');
		},
		bidSectionCheckItem(context) {
			loadMap(context, 'tb_bid_section_check_item', 'bidSectionCheckItemMap');
		},
		contractor(context) {
			loadMap(context, 'tb_contractor', 'contractorMap');
		},
		building(context) {
			loadMap(context, 'tb_building', 'buildingMap');
		},
		floor(context) {
			loadMap(context, 'tb_floor', 'floorMap');
		},
		unit(context) {
			loadMap(context, 'tb_unit', 'unitMap');
		},
		userProject(context) {
			loadRel(context, 'tb_user_project', 'userProjectRel', 'user_id', 'project_id', 'userMap', 'projectMap');
		},
		bidSectionContractor(context) {
			loadRel(context, 'tb_bid_section_contractor', 'bidSectionContractorRel', 
				'bid_section_id', 'contractor_id', 'bidSectionMap', 'contractorMap');
		},
		bidSectionCheckItem(context) {
			let sql = "select * from tb_bid_section_check_item";
			let bidSectionCheckItemRel = context.state.bidSectionCheckItemRel;
			window.db.executeSql(sql, [], (rs) => {
				for(let i = 0; i < rs.rows.length; i++) {
					let row = rs.rows.item(i);
					bidSectionCheckItemRel[row.id] = row;
					
					let bidSectionMap = context.state.bidSectionCheckItemRel.bidSectionMap;
					if(bidSectionMap[row.bid_section_id] == null) {
						bidSectionMap[row.bid_section_id] = [];
					}
					bidSectionMap[row.bid_section_id].push(row.id);
					
					let checkItemMap = context.state.bidSectionCheckItemRel.checkItemMap;
					if(checkItemMap[row.check_item_id] == null) {
						checkItemMap[row.check_item_id] = [];
					}
					checkItemMap[row.check_item_id].push(row.id);
				}
				console.log('store load finish: bidSectionCheckItem');
			}, error => {
				console.error(error);
			});
		},
		house(context) {
			let sql = 
				"select * from tb_house "
			;
			let projectId = getStore(keys.projectIdKey);
			window.db.executeSql(sql, [], (rs) => {
				var map = {
					floorUnitMap: {}
				};
				for(let i = 0; i < rs.rows.length; i++) {
					let row = rs.rows.item(i);
					map[row.id] = row;
					let unitId = row.unit_id;
					let floorId = row.floor_id;
					if(map.floorUnitMap[floorId + unitId] == null) {
						map.floorUnitMap[floorId + unitId] = [];
					}
					map.floorUnitMap[floorId + unitId].push(row);
				}
				context.state.houseMap = map;
				console.log('store load finish: house');
			}, error => {
				console.error(error);
			});
		},
		unitCheckItemRate(context){
			let sql = "select * from tmp_quality_measure_rate order by unit_id,item_id";
			window.db.executeSql(sql, [], (rs) => {
	        	for(var i = 0; i < rs.rows.length; i++) {
	        		var row = rs.rows.item(i);
	        		var unitObj = context.state.unitCheckItemRateMap[row.unit_id];
	        		if(unitObj == null) {
	        			unitObj = {};
	        			context.state.unitCheckItemRateMap[row.unit_id] = unitObj;
	        		}
	        		unitObj[row.item_id] = row.rate;
	        	}
	        	console.log('store load finish: unitCheckItemRateMap');
	        }, error => {
				console.error(error);
			});
		}
	}
}

function loadMap(context, tableName, mapName) {
	let sql = "select * from " + tableName;
	Common.executeSql(sql, []).then(result => {
		let rs = result.rs;
		var map = {};
		for(let i = 0; i < rs.rows.length; i++) {
			let row = rs.rows.item(i);
			map[row.id] = row;
		}
		context.state[mapName] = map;
		console.log('store load finish: ' + mapName);
	}).catch(error => {
		console.error(error);
	});
}

function loadRel(context, tableName, relName, idName1, idName2, mapName1, mapName2) {
	let sql = "select * from " + tableName;
	Common.executeSql(sql, []).then(result => {
		let rs = result.rs;
		var rel = {};
		rel[mapName1] = {};
		rel[mapName2] = {};
		for(let i = 0; i < rs.rows.length; i++) {
			let row = rs.rows.item(i);
			if(rel[mapName1][row[idName1]] == null) {
				rel[mapName1][row[idName1]] = [];
			}
			rel[mapName1][row[idName1]].push(row[idName2]);
			if(rel[mapName2][row[idName2]] == null) {
				rel[mapName2][row[idName2]] = [];
			}
			rel[mapName2][row[idName2]].push(row[idName1]);
		}
		context.state[relName] = rel;
		console.log('store load finish: ' + tableName);
	}).catch(error => {
		console.error(error);
	});
}

export default global;