import Vue from 'vue';
import Vuex from 'vuex';

import global from './modules/global';
import todo from './modules/todo';
import getters from './getters';

Vue.use(Vuex);

const store = new Vuex.Store({
	modules: {
		global,
        todo
	},
	getters
});

export default store;