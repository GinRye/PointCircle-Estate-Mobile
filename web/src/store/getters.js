const getters = {
	userMap: state => state.global.userMap,
	projectMap: state => state.global.projectMap
}
export default getters
